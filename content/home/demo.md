+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "white"
  
  # Background gradient.
  # gradient_start = "white"
  # gradient_end = "white"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

The NeuroModulation Laboratory is part of the [Brain & Behaviour Group](https://www.surrey.ac.uk/brain-and-behaviour-research-group) at the [University of Surrey](https://www.surrey.ac.uk/). We use multimodal tools to explore how the brain coordinates interactions between regions and how neuromodulators can shape those interactions. The lab is headed by [Dr. Ines Violante]({{< ref "../content/authors/admin/_index.md" >}}). 


Like an orchestra that relies on the coordinated efforts of its members, the brain depends on its many regions working together to perform the multitude of cognitive functions that makes us human. These functions allow us to solve problems, retrieve relevant information from memory and select the responses necessary to perform a particular task. In order to do this, the brain must coordinate the interactions between regions located far apart. 
One of the greatest challenges of modern neuroscience is to understand how these interactions occur, and how their occurrence gives rise to efficient behaviour. 

We are interested in developing and applying tools capable of influencing brain function and the interaction between brain regions. This could help scientists understand better how a particular pattern of brain activity is associated to efficient behaviour, such as being able to retain information in memory or solve a problem. Such tools could then be applied to neurological and psychiatric conditions, where the interactions between brain regions are malfunctioning.


