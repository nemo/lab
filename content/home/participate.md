+++

# A Participate section
widget = "custom"  # Replace with appropriate widget
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Participate"
subtitle = "Take part in our studies!"

# Here you can add content specific to the participation section.
# This could be a call-to-action, instructions, or details about ongoing projects.
# You can format this section similarly to how you would write a Markdown file.

[content]
  # Page type to display. E.g., a custom page type for participation.
  page_type = "participate"

  # You can also add filter buttons if needed, or remove them for a simple static content section.

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   5 = Showcase
  view = 1

  # Additional design settings can be adjusted as per your theme's documentation.

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

<!-- Anchor for the Participate menu item -->
<h2 id="participate">Participate in Our Projects</h2>

If you would like to take part in one of our studies and get to know our research by participating yourself, you can contact us to take part in the following ongoing studies. 
If you have any questions about participation and the project itself, please don't hesitate to ask!

## The following projects are available for participation:

- **Project 1**: Project 1
- **Project 2**: Project 2
- **Project 3**: Project 3

