---
# Display name
name: Valeria Jaramillo

# Is this the primary user of the site?
superuser: false

# Role/position
role: Postdoctoral Research Fellow

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/valeria-jaramillo"

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- REM sleep
- Closed-loop auditory stimulation

education:
  courses:
  - course: PhD 
    institution: ETH Zurich, external dissertation at University Children's Hospital Zurich
    year: 2020
  - course: MSc Neuroscience
    institution: ETH Zurich
    year: 2016
  - course: BSc Biochemistry
    institution: ETH Zurich
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:v.jaramillo@surrey.ac.uk'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=EA08nSQAAAAJ&hl=de&oi=ao
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.surrey.ac.uk/nemo/
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0003-1446-8328
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/val_jaramillo



# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Research Group
- Researchers
---


I am a Swiss National Science Foundation Postdoc Mobility Research Fellow working on the development of approaches that are able to module REM sleep oscillations in a non-invasive manner. I successfully employed closed-loop auditory stimulation to modulate REM sleep alpha and theta oscillations in healthy young adults. Since REM sleep EEG slowing is associated with cognitive decline in ageing and dementia I plan to test if this tool can also be used to modulate REM sleep oscillations in older individuals and in people living with dementia. I have recently obtained a Wellcome Early Career Award in which I aim to further investigate the mechanisms underlying restoration during REM sleep by performing multimodal assessments of dynamic REM sleep features and relating them to brain function in humans.