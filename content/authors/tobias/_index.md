---
# Display name
name: Tobias Raufeisen

# Username (this should match the folder name)
authors:
- tobias

# Is this the primary user of the site?
superuser: false

# Role/position
role: PhD Researcher

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/tobias-raufeisen"

# Short bio (displayed in user profile at end of posts)
bio: Test

interests:
  - Brain Stimulation
  - Sleep
  - Temporal Interference Stimulation
  - Simultaneous Brain Stimulation + HD-EEG 

education:
  courses:
  - course: RM Msc in Cognitive Neuroscience
    institution: Maastricht University
    year: 2023
  - course: BSc in Psychology
    institution: University of Muenster
    year: 2020

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:t.raufeisen@surrey.ac.uk'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/RaufeisenTobias


# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Research Group
- PhD Students

---

**The effect of temporal interference stimulation on sleep spindles**

My research project focuses on temporal interference stimulation (TI), a new neurostimulation technique severely underexplored in human research. Through this project I investigate the effects that TI has on sleep, specifically sleep spindles, as well as connected measures of sleep quality (e.g. sleep efficiency). 
