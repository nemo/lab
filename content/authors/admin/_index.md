---
# Display name
name: Ines R. Violante

# Is this the primary user of the site?
superuser: true

# Role/position
role: Principal Investigator, Senior Lecturer Psychological Neuroscience

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/ines-violante"

# Short bio (displayed in user profile at end of posts)
bio: .

interests:
- Neuromodulation
- Neuroimaging
- Brain dynamics
- Brain networks

education:
  courses:
  - course: PhD in Biomedical Sciences
    institution: University of Coimbra
    year: 2012
  - course: BSc in Biochemistry
    institution: University of Coimbra
    year: 2007

employment: test

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:ines.violante@surrey.ac.uk'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=c6TrSKgAAAAJ&hl=en
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.surrey.ac.uk/nemo/
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0002-4787-2901
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/inesviolante



# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Research Group
- Principal Investigator
---


I am a Senior Lecturer in Psychological Neuroscience and group leader of the NeuroModulation Lab.
My research programme is built on an interdisciplinary approach to cognitive and systems neuroscience focusing on understanding the mechanisms that govern brain network interactions, their role in human behaviour, and the application of that knowledge to develop approaches that benefit individuals with neurological and psychiatric conditions. 

