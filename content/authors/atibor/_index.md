---
# Display name
name: Tibor Auer

# Username (this should match the folder name)
authors:
- tibor

# Is this the primary user of the site?
superuser: false

# Role/position
role: Postdoctoral Researcher

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- neuroimaging
- magnetic resonance imaging 
- electroencephalography 
- biomarkers 
- research methods
- neuroinformatics
- research computing
- scalable data processing pipelines
- big data analysis
- open-source software development 

# education:
#   courses:
#   - course: PhD in Artificial Intelligence
#     institution: Stanford University
#     year: 2012
#   - course: MEng in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2009
#   - course: BSc in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:t.auer@surrey.ac.uk'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=PzLFHjUAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/tiborauer
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Former Members
#- Research Group

---

My research interests include assessing and improving reproducibility in neuroimaging, as well as developing approaches and tools to characterise the robustness of research findings and facilitate the adoption of best practices.

I received my undergraduate degree in medicine (MD) and my PhD in clinical neuroscience from the University of Pécs, Hungary, where I implemented a broad spectrum of in vivo MR techniques in a clinical environment. Then, I joined the methods-oriented Biomedizinische NMR Forschungs GmbH in Max-Planck Institute for Biophysical Chemistry in Göttingen, studied the assumptions and mechanisms underlying neurofeedback training (NFT), a non-invasive intervention, and optimised the experimental setup and protocol. At the MRC Cognition and Brain Sciences Unit in Cambridge and the Royal Holloway University of London in Egham, I led the development of Automatic Analysis, a reproducible and scalable neuroimaging analysis pipeline, and contributed to international harmonisation initiatives, including the Brain Imaging Data Structure and the Neuroimaging Data Model. Combining my commitment to clinically applicable research and my keen interest in powerful and reliable methods, I joined the University of Surrey, School of Psychology to investigate neurocognitive changes during healthy ageing and assess and optimise electric and neurofeedback-based neuromodulation.