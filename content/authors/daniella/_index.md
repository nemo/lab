---
# Display name
name: Daniella Jones

# Username (this should match the folder name)
authors:
- daniella

# Is this the primary user of the site?
superuser: false

# Role/position
role: PhD Candidate

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/danielle-louise-jones"

# Short bio (displayed in user profile at end of posts)
bio: 

interests: 
  - Neuroimaging
  - Hormones
  - Ageing
  - Dementia

techniques:
  - Magnetic Resonance Imaging
  - Magnetic Resonance Spectroscopy
  - Functional Magnetic Resonance Imaging

education:
  courses:
    - course: PhD in Psychology (Neuroscience)
      institution: University of Surrey
      year: Present
    - course: MSc in Psychology
      institution: University of Surrey
      year: 2019

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:daniellalouise.jones@surrey.ac.uk'
- icon: github
  icon_pack: fab
  link: https://github.com/daniellajones
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0001-6893-3673

  -icon: twitter
  icon_pack: fab
  link: https://twitter.com/DaniellaLJones
#- icon: google-scholar
 # icon_pack: ai
  #link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Research Group
- PhD Students

---

Understanding the effect of endocrine ageing on women’s brain chemistry, and as a risk factor for disease is of critical importance. Women are often overlooked or excluded from participant samples, which has prevented scientific understanding of how hormones impact the female brain. Oestrogen deprivation due to menopause is associated with several debilitating neurological symptoms, which can necessitate for hormone replacement treatment (HRT). My research involves the use of proton magnetic resonance spectroscopy, magnetic resonance imaging and functional magnetic resonance imaging to assess the HRT's influence on women's neurochemistry, brain morphology, and brain function at menopause. 