---
# Display name
name: Prince Okyere

# Username (this should match the folder name)
authors:
- Prince

# Is this the primary user of the site?
superuser: false

# Role/position
role: PhD Candidate

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/prince-okyere"


# Short bio (displayed in user profile at end of posts)
bio: 

interests:
  - Memory
  - Sleep
  - Temporal Interference Stimulation



education:
  courses:
  - course: MSc Cognitive Science (Neuroscience track)
    institution: University of Trento (Center for Mind and Brain)
    year: 2022
  - course: BSc Psychology
    institution: University of Cape Coast
    year: 2017



# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:p.okyere@surrey.ac.uk'
- icon: github
  icon_pack: fab
  link: https://github.com/p-okyere
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?view_op=new_articles&hl=en&imq=Prince+Okyere&inst=15262737669262836719#
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.surrey.ac.uk/nemo/


  #-icon: twitter
  #icon_pack: fab
  #link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
 # icon_pack: ai
  #link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Research Group
- PhD Students

---

I am a PhD student interested in using non-invasive neuromodulation approaches, such as Temporal Interference, to understand the mechanisms of memory consolidation in humans during sleep. During the course of my PhD, I will combine Temporal Interference Stimulation (TIS) with high-density EEG and MRI to study these mechanisms during sleep. 






