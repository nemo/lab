---
# Display name
name: Danielle Kurtin

# Username (this should match the folder name)
authors:
- danielle

# Is this the primary user of the site?
superuser: false

# Role/position
role: PhD student

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: "https://www.surrey.ac.uk/people/danielle-kurtin"
- name: Imperial College London
  url: "https://www.imperial.ac.uk/people/danielle.kurtin18"

# Short bio (displayed in user profile at end of posts)
bio: 

interests:




# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:d.kurtin@surrey.ac.uk'
- icon: github
  icon_pack: fab
  link: https://github.com/daniellekurtin
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0001-5368-6737

  #-icon: twitter
  #icon_pack: fab
  #link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
 # icon_pack: ai
  #link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ


# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
#- Research Group
#- PhD Students

---

I am a PhD student studying neural network dynamics. During the course of my PhD I will construct a computational model of the neural network dynamics underpinning different aspects of cognition using publicly available, multimodal neuroimaging data. I will use this model to suggest regions and parameters for non-invasive brain stimulation (NIBS). Then, by empirically testing the suggested stimulation parameters, I hope to lower the switch cost in task-switching paradigms. This could improve task performance, provide a deeper mechanistic understanding of NIBS's effects, and further expand the potential of this rapidly developing technique

**Projects:**
1. *Task switching paradigm*: The task switching game was built to be easily configured to the demands of different experiments. It was built to reduce visual confounds, and can be played during in-lab experiments, from an MR scanner, or remotely, as a web-based game. 

Play the game here - [Task switching paradigm](https://www.task-switching-game@surrey.ac.uk)

The code can be viewied here: https://github.com/daniellekurtin/task_switching_paradigm



