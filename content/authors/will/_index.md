---
# Display name
name: Will Mayes

# Username (this should match the folder name)
authors:
- will

# Is this the primary user of the site?
superuser: false

# Role/position
role: PhD student

# Organizations/Affiliations
organizations:
- name: University of Surrey
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
# - Artificial Intelligence
# - Computational Linguistics
# - Information Retrieval

# education:
#   courses:
#   - course: PhD in Artificial Intelligence
#     institution: Stanford University
#     year: 2012
#   - course: MEng in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2009
#   - course: BSc in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
# social:
# - icon: envelope
#   icon_pack: fas
#   link: 'mailto:test@example.org'
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/GeorgeCushen
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni

---

During my time at the University of Surrey, I obtained an undergraduate degree in Psychology, followed by an MSc in Research Methods in Psychology, which equipped me with practical experience in applying a diverse array of research methodologies.
Subsequently, I pursued a PhD at the University of Surrey, where I investigated Developmental Coordination Disorder (DCD), a common yet understudied neurodevelopmental condition with significant impacts on movement and daily living activities. Utilising neuroimaging and psychophysical task paradigms to explore the sensory aspects of the disorder, this research culminated in the identification of a potential biomarker in the form of elevated concentrations of GABA+ in adults with DCD.
Currently, I hold the position of Senior Researcher at the Care Quality Commission, where I oversee the adult inpatient and children and young person's surveys as part of the NHS patient surveys programme. I am extremely interested in further developing my analytical skills, which I developed during my time as a PhD student and have recently commenced studies in data science techniques as part of the Office for National Statistics' Data Science Graduate Programme.

