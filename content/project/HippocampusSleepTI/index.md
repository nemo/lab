---
title: Non-invasive Temporal Interference stimulation to explore the mechanism of memory consolidation during sleep. 
summary: Temporal Interference Stimulation targeting hippocampal ripples.
tags:
- sleep
- memory consolidation
- brain stimulation
date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

This project involves combining Temporal Interference Stimulation (TIS) with high-density EEG (HD-EEG) and MRI to target the hippocampus during sleep. By integrating these multimodal approaches with behavioural measures, [Prince']({{< ref "../../content/authors/prince/_index.md" >}})  aim to gain a deeper understanding of the hippocampal-cortical dialogue that facilitates memory consolidation. This approach could lead to the development of interventions designed to improve memory in individuals with conditions such as Alzheimer's disease.