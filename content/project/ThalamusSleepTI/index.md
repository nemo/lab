---
title: Enhancement of Sleep Quality through Neurostimulation
summary: Temporal Interference Stimulation targeting Sleep Spindles.
tags:
- sleep
- brain stimulation
date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

This project researches a new and exciting brain stimulation technique called "Temporal Interference Stimulation" (TI) and its effects on sleep. Specifically, the aim of [Tobias']({{< ref "../../content/authors/tobias/_index.md" >}}) project is to modulate density, duration and power of so-called "sleep spindles", which occur during restful sleep. Enhancing these electrophysiological markers through live brain stimulation is a promising alternative to pharmaceutical interventions against sleep disorders.