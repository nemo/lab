---
title: Can hormone replacement help protect the ageing brain?
summary: Neuroimaging investigation into the effects of HRT on brain chemistry, brain morphology, and brain function in postmenopausal women.
tags:
- ageing
- cognitive

date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

Hormone Replacement Therapy (HRT) might be instrumental in reducing women's Alzheimer's risk  - [Daniella's]({{< ref "../../content/authors/henry/_index.md" >}}) research employs a combination of neuroimaging techniques to provide a comprehensive understanding of how HRT influences the postmenopausal brain. By examining the intricate relationship between oestrogen levels and brain structure, function, and chemistry, this research aims to gain deeper insights into the potential role of HRT in preserving brain health as women age.
