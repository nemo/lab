---
title: Modulation of REM sleep oscillations using closed-loop auditory stimulation
summary: Closed-loop auditory stimulation targeting REM sleep oscillations.
tags:
- sleep
- brain stimulation

date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

REM sleep represents a paradoxical state in which brain activity appears similar to the activity observed during wakefulness, yet the body is completely immobile. Transient alpha and theta oscillations can be observed in REM sleep - but the function of these oscillations remains a mystery. [Valeria's]({{< ref "../../content/authors/valeria/_index.md" >}}) research aims to better understand how REM sleep oscillations contribute to cognitive function by modulating them using closed-loop auditory stimulation and evaluating effects on daytime functions.